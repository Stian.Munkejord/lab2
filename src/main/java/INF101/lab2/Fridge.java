package INF101.lab2;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {

    final int SIZE = 10;
    ArrayList<FridgeItem> test = new ArrayList<>(SIZE);

    @Override
    public int nItemsInFridge() {
        // TODO Auto-generated method stub
        return test.size();
    }

    @Override
    public int totalSize() {
        // TODO Auto-generated method stub
        return SIZE;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        // TODO Auto-generated method stub
        if (nItemsInFridge() < SIZE) {
            test.add(item);
            return true;
        }
        return false;
    }

    @Override
    public void takeOut(FridgeItem item) {
        // TODO Auto-generated method stub
        if (test.contains(item)) {
            test.remove(item);
            return;
        }
        throw new NoSuchElementException();
    }

    @Override
    public void emptyFridge() {
        // TODO Auto-generated method stub
        test.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        // TODO Auto-generated method stub
        List<FridgeItem> exp = new LinkedList<>();
        for (FridgeItem item : test) {
            if (item.hasExpired()) {
                takeOut(item);
                exp.add(item);
            }
        }
        // for(FridgeItem item : exp){
        //     takeOut(item);
        // }
        return exp;
    }

}
